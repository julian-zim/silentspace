﻿using System.Collections;
using UnityEngine;

public class TorchUsable : MonoBehaviour
{
     
     #region Constants

     private const float AttractionSpeed = 0.1f;
     
     private const float WeldStepSize = 0.005f;
     private const float WeldSpeed = 100f;
     
     #endregion
     
     #region Unity Variables
     
     [SerializeField] private AudioSource audioSource;
     
     [SerializeField] private FPC fpcScript;
     [SerializeField] private GameObject player;
     [SerializeField] private Camera playerCamera;
     
     [SerializeField] private GameObject torchModel;
     [SerializeField] private ParticleSystem ray;
     [SerializeField] private ParticleSystem sparks;
     
     [SerializeField] private GameObject weldZone;
     [SerializeField] private GameObject door;

     [SerializeField] private Transform topLeft;
     [SerializeField] private Transform topRight;
     [SerializeField] private Transform bottomLeft;
     [SerializeField] private Transform bottomRight;
     
     #endregion

     #region Public Variables
     
     [HideInInspector] public bool inWeldZone;
     
     #endregion
     
     #region Private Variables
     
     private bool hasTorch;
     private bool selectedTorch;
     private bool startWeldingNextFrame;
     private bool isWelding;
     private float weldProgress;
     
     #endregion

     private void Update()
     {

          #region Coroutines
          
          if (startWeldingNextFrame)
          {
               startWeldingNextFrame = false;
               isWelding = true;
               StartCoroutine(WeldProcess());
               if (!GameData.airDrained)
                    StartCoroutine(RaiseAlienAttraction());
          }
          
          #endregion

          #region Start Progess

          if (hasTorch)
          {
               if (Input.mouseScrollDelta.y > 0)
                    SetActive(true);
               else if (Input.mouseScrollDelta.y < 0)
                    SetActive(false);

               if (Input.GetKeyDown(KeyCode.E) && selectedTorch && inWeldZone)
               {
                    fpcScript.controlsEnabled = false;
                    inWeldZone = false;      // not very smoothly solved
                    weldZone.GetComponent<WeldZone>().ResetDoorColor();
                    startWeldingNextFrame = true;
               }
          }

          #endregion

          #region Cancel Progess

          if (Input.GetKeyDown(KeyCode.E) && isWelding)
               CancelWeldProcess();
          
          #endregion
       
     }

     #region Helper Functions
     
     private void SetActive(bool active)
     {
          selectedTorch = active;
          torchModel.SetActive(active);
     }

     #endregion

     #region Public Functions
     
     // more like "give torch"
     public void GetTorch()
     {
          hasTorch = true;
          SetActive(true);
     }
     
     public void CancelWeldProcess()
     {
          if (isWelding)
          {
               StopAllCoroutines();
          
               Vector3 cameraRotation = playerCamera.transform.rotation.eulerAngles;
               fpcScript.xRotation = cameraRotation.x - (cameraRotation.x > 90f ? 360f : 0f);
               player.transform.rotation = Quaternion.Euler(0f, cameraRotation.y, 0f);
               fpcScript.controlsEnabled = true;
          
               ray.Stop();
               sparks.gameObject.SetActive(false);
               weldProgress = 0.0f;
          
               isWelding = false;
          }
     }

     #endregion

     #region Coroutines
     
     private IEnumerator WeldProcess()
     {
          
          Transform sparksTransform = sparks.transform;
          Transform cameraTransform = playerCamera.transform;

          // particle
          ray.Play();
          sparks.gameObject.SetActive(true);

          // weld
          Vector3 startPos = topLeft.position;
          Vector3 endPos = topRight.position;
          byte segment = 0;
          while (segment < 4)
          {
               weldProgress += WeldStepSize;
               sparksTransform.position = Vector3.Lerp(startPos, endPos, weldProgress);
               playerCamera.transform.rotation = Quaternion.LookRotation(sparksTransform.position - cameraTransform.position, Vector3.up);

               if (weldProgress >= 1.0f)
               {
                    segment++;
                    startPos = endPos;
                    switch (segment) {
                         case 1:
                              endPos = bottomRight.position;
                              break;
                         case 2:
                              endPos = bottomLeft.position;
                              break;
                         case 3:
                              endPos = topLeft.position;
                              break;
                    }
                    weldProgress = 0.0f;
               }
               
               yield return new WaitForSeconds(1f / WeldSpeed);
          }

          // orientation
          Vector3 cameraRotation = cameraTransform.rotation.eulerAngles;
          fpcScript.xRotation = cameraRotation.x - (cameraRotation.x > 90f ? 360f : 0f);
          player.transform.rotation = Quaternion.Euler(0f, cameraRotation.y, 0f);
          fpcScript.controlsEnabled = true;
          
          // finish welding
          ray.Stop();
          Destroy(weldZone);       // includes sparks
          door.GetComponent<Rigidbody>().isKinematic = false;
          isWelding = false;

          // end game
          if (GameData.hasHelmet)
               EndGame.instance.WinGame();
          else      // actually unreachable
               EndGame.instance.LoseGame("Bruh how did you even do that?!");
     }

     private IEnumerator RaiseAlienAttraction()
     {
          while (true)
          {
               Alien.instance.Attract(AttractionSpeed);
               yield return new WaitForSeconds(0.1f);
          }
     }
     
     #endregion
     
}
