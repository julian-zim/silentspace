﻿using System.Collections;
using UnityEngine;

public class HelmetUsable : MonoBehaviour
{
    
    #region Constants
    
    private const float AttractFactor = 0.75f;
    
    private const float TransitionDelay = 1.0f;
    private const float TransitionDuration = 1.0f;
    private const float VeilDuration = 1.0f;
    private const float VeilSpeed = 0.5f;
    
    #endregion

    #region Unity Variables
    
    [SerializeField] private ChangeSnapshot changeSnapshotScript;
    [SerializeField] private GameObject helmetModel;
    
    #endregion

    #region Private Variables
    
    private AudioSource audioSource;
    private Texture2D veil;
    private bool applyVeil;
    
    #endregion
    
    private void Start()
    {
        
        #region Variable Initializtion
        
        audioSource = GetComponent<AudioSource>();
        
        #endregion
        
        #region Initializtion
        
        veil = new Texture2D(1, 1);
        veil.SetPixel(0, 0, new Color(0, 0, 0, 0));
        veil.Apply();
        
        #endregion
        
    }

    #region Public Functions
    
    // inspired by https://www.youtube.com/watch?v=a66Fa7Oh7TE
    public void EquipHelmet(GameObject toDestroy)
    {
        
        Utils.instance.MakeSound(audioSource, true, AttractFactor);
        changeSnapshotScript.SwitchToCorrectSnapshot(TransitionDelay, TransitionDuration);
        StartCoroutine(BlackOutCoroutine(toDestroy, VeilDuration, VeilSpeed));
        
    }
    
    #endregion

    #region Helper Functions

    private void OnGUI()
    {
        if (applyVeil)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), veil);
    }

    #endregion
    
    #region Coroutines

    private IEnumerator BlackOutCoroutine(GameObject toDestroy, float duration, float speed)
    {
        
        float sinceStart = 0f;
        applyVeil = true;
        while (duration > (sinceStart += Time.deltaTime))
        {
            float alpha = Mathf.Min(sinceStart / speed, 1f);
            veil.SetPixel(0, 0, new Color(0, 0, 0, alpha));
            veil.Apply();
            yield return null;
        }
        sinceStart = 0;
        helmetModel.SetActive(true);

        Destroy(toDestroy);

        while (duration > (sinceStart += Time.deltaTime))
        {
            float alpha = Mathf.Min(sinceStart / speed, 1f);
            veil.SetPixel(0, 0, new Color(0, 0, 0, 1 - alpha));
            veil.Apply();
            yield return null;
        }
        applyVeil = false;
        
    }


    #endregion
    
}
