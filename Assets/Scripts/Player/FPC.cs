﻿using UnityEngine;
using UnityEngine.UI;

public class FPC : MonoBehaviour
{

    #region Constants
    
    private const float Sensivity = 300f;
    private const float Speed = 5f;
    private const float JumpHeight = 1f;
    private const float InteractRange = 3f;

    #endregion

    #region Unity Variables

    [SerializeField] private Camera mainCamera;
    [SerializeField] private Image crosshair;
    
    #endregion

    #region Public Variables
    
    [HideInInspector] public float gravity = -9.81f;      // normally a constant. probably only temporarily a variable.
    [HideInInspector] public bool controlsEnabled;
    [HideInInspector] public bool onGround;
    [HideInInspector] public float xRotation;
    
    #endregion

    #region Private Variables

    private CharacterController controller;
    private Vector3 direction;
    private float fallVelocity;
    
    #endregion
    
    private void Start()
    {
        
        #region Variable Initialization

        controller = GetComponent<CharacterController>();
        controlsEnabled = true;

        #endregion

        #region Initialization
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        #endregion

    }
    
    private void Update()
    {

        if (controlsEnabled)
        {
            
            #region Rotation
            
            transform.Rotate(0f, Input.GetAxis("Mouse X") * Sensivity * Time.deltaTime, 0f);
            xRotation = Mathf.Clamp(xRotation - Input.GetAxis("Mouse Y") * Sensivity * Time.deltaTime, -90f, 90f);
            mainCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            #endregion
            
            #region Movement

            direction = (transform.right * Input.GetAxisRaw("Horizontal") + transform.forward * Input.GetAxisRaw("Vertical")).normalized;
            controller.Move(direction * (Speed * Time.deltaTime));
            
            if (onGround && fallVelocity < 0)
                fallVelocity = 0;
            if (Input.GetKeyDown(KeyCode.Space) && onGround)
            {
                fallVelocity = Mathf.Sqrt(JumpHeight * -2f * gravity);
            }
            fallVelocity += gravity * Time.deltaTime;
            controller.Move(new Vector3(0f, fallVelocity * Time.deltaTime, 0f));

            #endregion

            #region Interaction
            
            GameObject interactableObject = GetInteractable();
            if (interactableObject)
            {
                crosshair.color = new Color(1.0f, 0.8f, 0.3f, 1.0f);
                if (Input.GetKeyDown(KeyCode.E))
                    interactableObject.GetComponent<Interactable>().Interact();
            }
            else
            {
                crosshair.color = new Color(0.8f, 0.8f, 0.8f, 1.0f);
            }
            
            #endregion

        }
        
    }
    
    #region Public Functions

    public void BonkIntoCeiling()
    {

        fallVelocity = -2f;

    }

    #endregion

    #region Helper Functions
    
    private GameObject GetInteractable()
    {
        if (Physics.Raycast(mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), out var hit, InteractRange) && hit.collider.CompareTag("Interactable"))
            return hit.collider.gameObject;
        return null;
    }
    
    #endregion
    
}
