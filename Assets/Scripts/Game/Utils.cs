﻿using UnityEngine;
using UnityEngine.Audio;

public class Utils : MonoBehaviour
{

    #region Singleton Pattern

    public static Utils instance;
    
    private void Awake()
    {
        instance = this;
    }

    #endregion
    
    // plays the audio of audioSource with the given volume and attracts the alien based on (volume * attractIntensity)
    public void MakeSound(AudioSource source, bool interrupt, float volume, float attractFactor)
    {
        source.volume = volume;
        
        if (interrupt)
            source.Play();
        else
            source.PlayOneShot(source.clip);
        
        Alien.instance.Attract(volume * attractFactor);
    }
    
    public void MakeSound(AudioSource source, bool interrupt, float attractFactor)
    {
        if (interrupt)
            source.Play();
        else
            source.PlayOneShot(source.clip);
        
        Alien.instance.Attract(source.volume * attractFactor);
    }
    
    // just there so audioSource.Play() is never used (except for maybe music) and so where scripts play sounds can be tracked back 
    public void MakeSound(AudioSource source, bool interrupt)
    {
        if (interrupt)
            source.Play();
        else
            source.PlayOneShot(source.clip);
    }
    
    // same as above but with only one parameter so it can be called by a unity event
    public void MakeSound(AudioSource source)
    {
        source.Play();
    }

}
