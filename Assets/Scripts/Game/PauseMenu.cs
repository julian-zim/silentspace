﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour
{
    
    #region Unity Variables
    
    [SerializeField] private AudioMixer audioMixer;
    
    [SerializeField] private GameObject escMenu;
    [SerializeField] private Slider sliderMaster;
    [SerializeField] private Slider sliderAmbient;
    [SerializeField] private Slider sliderMusic;
    
    [SerializeField] private FPC fpcScript;
    
    #endregion

    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escMenu.SetActive(!escMenu.activeSelf);
            fpcScript.controlsEnabled = !escMenu.activeSelf;

            if (!escMenu.activeSelf)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                // normal sh
            }
            if (escMenu.activeSelf)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                // menu sh
            }

        }

        if (escMenu.activeSelf)
        {
            audioMixer.SetFloat("MainVolume", Mathf.Log10(sliderMaster.value) * 20);
            audioMixer.SetFloat("AmbienceVolume", Mathf.Log10(sliderAmbient.value) * 20);
            audioMixer.SetFloat("MusicVolume", Mathf.Log10(sliderMusic.value) * 20);
        }

    }

}
