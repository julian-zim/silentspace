﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Alien : MonoBehaviour
{
    
    #region Singleton Pattern
    
    public static Alien instance;
    
    private void Awake()
    {
        instance = this;
    }
    
    #endregion

    #region Constants

    public const float FalloffSpeed = 10f;
    private const float FalloffValue = 0.05f;
    private readonly float[] lvlThreshholds = {1f, 2f, 3f};
    private readonly Vector3 alienStartPos = new Vector3(0f, -1.56f, 1.49f);
    private readonly Vector3 alienEndPos = new Vector3(0, -0.56f, 0.49f);
    
    #endregion

    #region Unity Variables
    
    [SerializeField] private AudioSource musicAudioSource;
    [SerializeField] private AudioSource deathAudioSource;
    [SerializeField] private Text attractionPrompt;
    [SerializeField] private GameObject alienModel;
    [SerializeField] private AlienSounds alienSoundsScript;
    [SerializeField] private TorchUsable torchScript;
    [SerializeField] private FPC fpcScript;
    
    #endregion

    #region Private Variables

    private bool active;
    private float attractionValue;
    private int dangerLevel;
    private int lockLevel;      // while level x reaction sound is played, block all level <x reactions

    #endregion
    
    #region Event Functions
    
    private void Start()
    {
        StartCoroutine(InitialDelay());
        StartCoroutine(AttractionFalloff());
    }
    
    private void Update()
    {
        //attractionPrompt.text = "" + Mathf.Round(attractionValue * 100f) * 0.01f;
    }

    #endregion
    
    #region Private Functions
  
    private void AlienReaction(int level)
    {
        if (level > lockLevel)
        {
            lockLevel = level;
            
            alienSoundsScript.StopAllCoroutines();      // stops current alien sound
            alienSoundsScript.StartSound(level);
            
            if (level == 3)
            {
                StopAllCoroutines();    // stops AttractionFalloff
                attractionValue = 3f;

                musicAudioSource.Stop();
                deathAudioSource.PlayOneShot(deathAudioSource.clip);
                StartCoroutine(DelayedDeath());
            }
        }
    }
    
    #endregion
    
    #region Public Functions
    
    public void Attract(float intensity)
    {
        if (active && !GameData.airDrained)
            attractionValue += intensity;
        
        if (attractionValue > lvlThreshholds[2])
            attractionValue = lvlThreshholds[2];
            
        if (attractionValue >= lvlThreshholds[2])
            AlienReaction(3);
        else if (attractionValue >= lvlThreshholds[1])
            AlienReaction(2);
        else if (attractionValue >= lvlThreshholds[0])
            AlienReaction(1);
    }

    public void ResetLockLevel()
    {
        lockLevel = 0;
    }
    
    public float GetAttractionValue()
    {
        return attractionValue;
    }

    #endregion
    
    #region Coroutines

    private IEnumerator AttractionFalloff()
    {
        while (true)
        {
            if (attractionValue > 0)
            {
                attractionValue -= FalloffValue;
                if (attractionValue < 0)
                    attractionValue = 0;
            }
            yield return new WaitForSeconds(1f / FalloffSpeed);
        }
    }
    
    private IEnumerator DelayedDeath()
    {
        yield return new WaitForSeconds(AlienSounds.ReactionDelay);
        
        torchScript.CancelWeldProcess();
        fpcScript.controlsEnabled = false;
        
        alienModel.SetActive(true);
        alienModel.GetComponent<Animator>().SetTrigger(Animator.StringToHash("Attack"));
        alienModel.transform.localPosition = alienStartPos;

        float proximity = 0f;
        while (proximity < 1f)
        {
            alienModel.transform.localPosition = Vector3.Lerp(alienStartPos, alienEndPos, proximity);
            proximity += 0.1f;
            yield return new WaitForSeconds(0.02f);
        }

        EndGame.instance.LoseGame("The alien found you.");
    }

    private IEnumerator InitialDelay()
    {
        yield return new WaitForSeconds(1f);
        active = true;
    }

    #endregion

}
