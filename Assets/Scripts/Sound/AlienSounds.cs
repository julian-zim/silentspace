﻿using System.Collections;
using UnityEngine;

public class AlienSounds : MonoBehaviour
{

    #region Constants
    
    public const float ReactionDelay = 2f;
    private const float Distance = 40f;
    private const float ReactionCooldown = 3f;
    
    #endregion
    
    #region Unity Variables
    
    [SerializeField] private AudioClip[] audioClips;    // TODO: dynamic clip selection
    [SerializeField] private GameObject player;
    
    #endregion
    
    #region Private Variables
    
    private enum RelativePosition
    {
        FrontLeft,
        FrontRight,
        BackRight,
        BackLeft
    }
    
    private AudioSource audioSource;
    private RelativePosition lastPosition = RelativePosition.BackLeft;
    private int oldIndex = -1;

    #endregion

    private void Start()
    {

        #region Variable Initialization
        
        audioSource = GetComponent<AudioSource>();
        
        #endregion
        
        #region Initialization
        
        audioSource.maxDistance = 4f/3f * Distance;
        
        #endregion
        
    }

    #region Helper Functions
    
    private RelativePosition GetNextPosition()
    {
        RelativePosition position = RelativePosition.BackLeft;
        
        switch (lastPosition)
        {
            case RelativePosition.FrontLeft:
            {
                RelativePosition[] possiblePositons = {RelativePosition.FrontRight, RelativePosition.BackLeft};
                position = possiblePositons[Random.Range(0, 2)];
                break;
            }
            case RelativePosition.FrontRight:
            {
                RelativePosition[] possiblePositons = {RelativePosition.BackRight, RelativePosition.FrontLeft};
                position = possiblePositons[Random.Range(0, 2)];
                break;
            }
            case RelativePosition.BackRight:
            {
                RelativePosition[] possiblePositons = {RelativePosition.BackLeft, RelativePosition.FrontRight};
                position = possiblePositons[Random.Range(0, 2)];
                break;
            }
            case RelativePosition.BackLeft:
            {
                RelativePosition[] possiblePositons = {RelativePosition.FrontLeft, RelativePosition.BackRight};
                position = possiblePositons[Random.Range(0, 2)];
                break;
            }
        }
        
        lastPosition = position;
        return position;
    }

    private Vector3 EnumToVector(RelativePosition relativePosition)
    {
        Vector3 position = new Vector3();
        
        switch (relativePosition)
        {
            case RelativePosition.FrontLeft:
                position = (Vector3.forward - Vector3.right).normalized;
                break;
            case RelativePosition.FrontRight:
                position = (Vector3.forward + Vector3.right).normalized;
                break;
            case RelativePosition.BackRight:
                position = (-Vector3.forward + Vector3.right).normalized;
                break;
            case RelativePosition.BackLeft:
                position = (-Vector3.forward - Vector3.right).normalized;
                break;
        }

        return position;
    }
    
    #endregion  
    
    #region Public Functions
    
    public void StartSound(int level)
    {
        StartCoroutine(PlayAlienSound(level));
    }
    
    #endregion
    
    #region Coroutines
    
    private IEnumerator PlayAlienSound(int level)
    {
        yield return new WaitForSeconds(ReactionDelay);
        //Debug.Log("Playing alien sound level " + level + " after delay.");
        
        if (GameData.postSecondDoor)
            transform.position = player.transform.position + EnumToVector(GetNextPosition()) * (Distance - level * Distance / 3);
        
        int index;
        do
        {
            index = Random.Range(0, audioClips.Length);
        } while (oldIndex == index);
        oldIndex = index;
        audioSource.clip = audioClips[index];
        
        audioSource.Play();

        if (level < 3)
        {
            yield return new WaitForSeconds(ReactionCooldown);
            Alien.instance.ResetLockLevel();
        }
    }
    
    #endregion

}
