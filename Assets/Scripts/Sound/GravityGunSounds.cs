﻿using System.Collections;
using UnityEngine;

public class GravityGunSounds : MonoBehaviour
{

    #region Constants

    private const float UseSoundAttractFactor = 2.5f;
    private const float HumSoundAttractFactor = 0.1f;
    private const float HumSoundVolume = 0.1f;
    
    #endregion
    
    #region Unity Variables

    [SerializeField] private AudioClip grabSound;
    [SerializeField] private AudioClip releaseSound;
    [SerializeField] private AudioSource useSoundSource;
    
    #endregion
    
    #region Public Variables
    
    public AudioSource humSoundSource;

    #endregion

    #region Private Variables

    private GravityGunUsable gravityGunScript;
    private Rigidbody gravityGunRigidBody;
    private Vector3 lastGravityGunPosition;
    
    #endregion

    private void Start()
    {

        #region Variable Intialization

        gravityGunScript = GetComponent<GravityGunUsable>();
        gravityGunRigidBody = GetComponent<Rigidbody>();
        lastGravityGunPosition = gravityGunRigidBody.transform.position;

        #endregion

        #region Initialization

        humSoundSource.volume = HumSoundVolume;        

        #endregion
        
        #region Initial Calls
        
        humSoundSource.Play();
        StartCoroutine(HumAttraction());

        #endregion

    }

    private void Update()
    {

        #region Hum Sounds

        if (gravityGunScript.GetGrabbedObject())
        {
            Vector3 currentGravityGunPosition = gravityGunRigidBody.transform.position;
            float velocity = (lastGravityGunPosition - currentGravityGunPosition).magnitude;
            lastGravityGunPosition = currentGravityGunPosition;

            humSoundSource.pitch = 1f + Mathf.Clamp(velocity * 10f, 0f, 1f) * 0.5f;     // a pitch > 1.5 sounds horrible
            humSoundSource.volume = HumSoundVolume + Mathf.Clamp(velocity * 10f, 0f, 1f) * (1f - HumSoundVolume);
        }
        
        #endregion
        
    }

    #region Public Functions
    
    public void PlayGrabSound()
    {
        useSoundSource.clip = grabSound;
        Utils.instance.MakeSound(useSoundSource, true, UseSoundAttractFactor);
    }
    
    public void PlayReleaseSound()
    {
        useSoundSource.clip = releaseSound;
        Utils.instance.MakeSound(useSoundSource, true, UseSoundAttractFactor);
    }

    public void ResetHumSoundSource()
    {
        humSoundSource.pitch = 1.0f;
        humSoundSource.volume = HumSoundVolume;
    }

    #endregion

    #region Coroutines

    private IEnumerator HumAttraction()
    {
        while (true)
        {
            if (humSoundSource.volume > HumSoundVolume)
                Alien.instance.Attract(humSoundSource.volume * HumSoundAttractFactor);
            yield return new WaitForSeconds(1f / Alien.FalloffSpeed);
        }
    }

    #endregion
    
}
