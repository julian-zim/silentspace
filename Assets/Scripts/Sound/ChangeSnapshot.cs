﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class ChangeSnapshot : MonoBehaviour
{

    #region Singelton Pattern
    
    public static ChangeSnapshot instance;

    private void Awake()
    {
        instance = this;
    }
    
    #endregion

    #region Unity Variables

    [SerializeField] private AudioMixer mixer;
    [SerializeField] private AudioMixerSnapshot snapshotAir;
    [SerializeField] private AudioMixerSnapshot snapshotAirHelmet;
    [SerializeField] private AudioMixerSnapshot snapshotVacuum;
    [SerializeField] private AudioMixerSnapshot snapshotVacuumHelmet;
    
    #endregion

    private void Start()
    {
        GameData.Reset();
        SwitchToCorrectSnapshot(0, 0);
    }

    #region Public Functions
    
    public void SwitchToCorrectSnapshot(float delay, float duration)
    {
        AudioMixerSnapshot snapshot;
        if (GameData.airBeingDrained || GameData.airDrained)
            snapshot = GameData.hasHelmet ? snapshotVacuumHelmet : snapshotVacuum;
        else
            snapshot = GameData.hasHelmet ? snapshotAirHelmet : snapshotAir;

        StartCoroutine(TransitionToSnapshot(delay, duration, snapshot));
    }

    #endregion

    #region Coroutines

    private IEnumerator TransitionToSnapshot(float delay, float duration, AudioMixerSnapshot snapshot)
    {
        yield return new WaitForSeconds(delay);
        mixer.TransitionToSnapshots(new [] { snapshot }, new [] { 1f }, duration);
    }
    
    #endregion

}
