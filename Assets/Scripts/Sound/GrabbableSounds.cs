﻿using System.Collections;
using UnityEngine;

public class GrabbableSounds : MonoBehaviour
{
    
    #region Constants

    private const float VelVolRatio = 0.25f;
    
    #endregion
    
    #region Unity Variables
    
    [SerializeField] private float attractIntensity;      // set for every object individually
    [SerializeField] private AudioClip[] audioClips;
    [SerializeField] private GravityGunUsable gravityGunScript;
    
    #endregion

    #region Private Variables

    private AudioSource audioSource;
    private bool inCollisionDelay;
    
    #endregion
    
    private void Start()
    {

        #region Variable Initialization

        audioSource = GetComponent<AudioSource>();

        #endregion
        
    }

    #region Event Functions

    private void OnCollisionEnter(Collision collision)
    {
        GameObject grabbedObject = gravityGunScript.GetGrabbedObject();
        
        if (!inCollisionDelay && (!grabbedObject || !grabbedObject.Equals(gameObject)))
        {
            inCollisionDelay = true;
            StartCoroutine(CollideDelay());
            
            PlayCollisionSound(collision.relativeVelocity.magnitude);
        }

    }
    
    private void PlayCollisionSound(float impactVelocity)
    {

        if (impactVelocity < 4)
            audioSource.clip = audioClips[0];
        else if (impactVelocity < 8)
            audioSource.clip = audioClips[1];
        else
            audioSource.clip = audioClips[2];

        Utils.instance.MakeSound(audioSource, false, impactVelocity * VelVolRatio, attractIntensity);
        
    }

    #endregion
    
    #region Coroutines    

    private IEnumerator CollideDelay()
    {
        yield return new WaitForSeconds(0.01f);
        inCollisionDelay = false;
    }
    
    #endregion
    
}
