﻿using UnityEngine;

public class TorchInteractable : Interactable
{
    
    #region Unity Variables
    
    [SerializeField] private TorchUsable torchScript;
    [SerializeField] private GravityGunUsable gravityGunScript;
    
    #endregion
    
    public override void Interact()
    {
        
        gravityGunScript.LoseGravityGun();
        torchScript.GetTorch();
        Destroy(gameObject);
        
    }
    
}
