﻿using UnityEngine;

public class HelmetInteractable : Interactable
{
    
    #region Unity Variables
    
    [SerializeField] private HelmetUsable helmetScript;
    
    #endregion

    public override void Interact()
    {
        
        GameData.hasHelmet = true;
        helmetScript.EquipHelmet(gameObject);
        
    }
    
}
