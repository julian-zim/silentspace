﻿using UnityEngine;

public class LightToggle : MonoBehaviour
{
    
    #region Constants
    
    private const float Intensity = 8f;
    
    #endregion
    
    #region Private Variables
    
    private Light lightSource;

    #endregion
    
    private void Start()
    {
        lightSource = GetComponent<Light>();
    }

    // called by unity
    public void Toggle()
    {
        lightSource.intensity = lightSource.intensity > 0 ? 0 : Intensity;
    }
    
}
