﻿using UnityEngine;

public class FlickeringLight : MonoBehaviour
{
    
    #region Unity Variables
    
    [SerializeField] private AudioClip shortestFlicker;
    [SerializeField] private AudioClip shortFlicker;
    [SerializeField] private AudioClip mediumFlicker;
    [SerializeField] private AudioClip longFlicker;
    
    #endregion

    #region Private Variables
    
    private AudioSource audioSource;
    private Light lightSource;
    
    private float onDuration;
    private float offDuration;
    
    #endregion
    
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        lightSource = GetComponent<Light>();
        
        lightSource.enabled = true;
        SetNextDurations();
    }

    private void Update()
    {
        
        if (onDuration > 0) {
            
            onDuration -= Time.deltaTime;
            
            if (onDuration <= 0) {
                audioSource.clip = GetClip(offDuration);
                Utils.instance.MakeSound(audioSource, true);
                lightSource.enabled = false;
            }
            
        }
        else {
            
            offDuration -= Time.deltaTime;
            
            if (offDuration <= 0) {
                SetNextDurations();
                lightSource.enabled = true;
            }
            
        }
        
    }
    
    private void SetNextDurations() {
        
        onDuration = Random.value * Random.value * 5;
        offDuration = Random.value * Random.value * Random.value * 2;
        
    }
    
    private AudioClip GetClip(float offTime)
    {
        if (offTime < 0.05)
            return shortestFlicker;
        if (offTime < 0.15)
            return shortFlicker;
        if (offTime < 0.4)
            return mediumFlicker;
        return longFlicker;
    }

}
