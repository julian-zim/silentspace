﻿using UnityEngine;

public class AlarmLight : MonoBehaviour
{

    #region Constants

    private const float AttractFactor = 1.5f;
    private const float LightIntensity = 8f;
    private const float RotationSpeed = 0.2f;
    
    #endregion

    #region Private Variables
    
    private AudioSource audioSource;
    private Light lightSource;

    #endregion

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        lightSource = GetComponent<Light>();
    }

    private void Update()
    {
        transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * 360f * RotationSpeed);
    }

    // called by unity
    public void Toggle()
    {
        Utils.instance.MakeSound(audioSource, true, AttractFactor);
        lightSource.intensity = lightSource.intensity > 0 ? 0 : LightIntensity;
    }
    
}
