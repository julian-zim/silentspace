﻿using UnityEngine;

public class WeldZone : MonoBehaviour
{
    
    #region Constants
    
    private readonly int emissionColor = Shader.PropertyToID("_EmissionColor");

    #endregion
    
    #region Unity Variables
    
    [SerializeField] private TorchUsable torchScript;
    [SerializeField] private MeshRenderer doorRenderer;
    
    #endregion

    #region Private Variables

    private Color originalColor;

    #endregion
    
    #region Public Functions
    
    public void ResetDoorColor()
    {
        doorRenderer.material.SetColor(emissionColor, originalColor);
    }

    #endregion

    #region Event Functions
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            originalColor = doorRenderer.material.GetColor(emissionColor);
            doorRenderer.material.SetColor(emissionColor, new Color(0.1f, 0f, 0f));
            torchScript.inWeldZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            doorRenderer.material.SetColor(emissionColor, originalColor);
            torchScript.inWeldZone = false;
        }
    }

    #endregion
    
}
