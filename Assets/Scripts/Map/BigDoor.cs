﻿using System.Collections;
using UnityEngine;

public class BigDoor : MonoBehaviour
{
    
    #region Constants

    private const float AttractFactor = 0.6f;
    
    #endregion
    
    #region Unity Variables
    
    [SerializeField] private AudioClip[] soundOpen;
    [SerializeField] private AudioClip[] soundClose;

    [SerializeField] private AudioClip[] soundsFollowUp;            // *
    [SerializeField] private AudioClip soundScream;                 // *
    [SerializeField] private AudioSource lockedDoorAudioSource;     // only used by second door
    
    [SerializeField] private bool isFirstDoor;
    [SerializeField] private bool isSecondDoor;
    
    #endregion

    #region Private Variables
    
    private AudioSource audioSource;
    private Animator animator;
    private bool state;
    
    #endregion
    
    private void Start()
    {
        
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

    }
    
    // called by unity
    public void Toggle()
    {
        state = !state;
        animator.SetBool(Animator.StringToHash("open"), state);
        StartCoroutine(PlayDoorSequence(state ? soundOpen : soundClose));
    }
    
    private IEnumerator PlayDoorSequence(AudioClip[] sounds)
    {
        
        foreach (AudioClip audioClip in sounds)
        {
            audioSource.clip = audioClip;
            Utils.instance.MakeSound(audioSource, false, isFirstDoor ? 1f : AttractFactor);

            yield return new WaitWhile(() => audioSource.isPlaying);
        }

        if (lockedDoorAudioSource)
        {
            foreach (AudioClip audioClip in soundsFollowUp)
            {
                lockedDoorAudioSource.clip = audioClip;
                Utils.instance.MakeSound(lockedDoorAudioSource, false, AttractFactor + 0.2f);

                yield return new WaitWhile(() => lockedDoorAudioSource.isPlaying);
            }

            // I don't think this is good gameplaywise
            //lockedDoorAudioSource.clip = soundScream;
            //Utils.instance.MakeSound(lockedDoorAudioSource, false);
        }

        if (isSecondDoor)
            GameData.postSecondDoor = true;
        
    }
    
}
