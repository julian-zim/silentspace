﻿using System.Collections;
using HutongGames.PlayMaker.Actions;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class AirSystem : MonoBehaviour
{

    #region Constants

    private const float AttractFactor = 0.2f;    

    #endregion

    #region Unity Variables

    [SerializeField] private PostProcessVolume postProcess;
    
    #endregion
    
    #region Private Variables
    
    private AudioSource audioSource;

    #endregion

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // called by unity
    public void Toggle() {
        
        Utils.instance.MakeSound(audioSource, false, AttractFactor);
        
        GameData.airBeingDrained = true;
        ChangeSnapshot.instance.SwitchToCorrectSnapshot(1.0f, 10f);
        
        StartCoroutine(WaitForAirDrained());
        
    }

    private IEnumerator WaitForAirDrained()
    {

        if (GameData.hasHelmet)
            yield return new WaitForSeconds(5f);
        else
        {
            postProcess.profile.TryGetSettings(out DepthOfField dof);
            dof.active = true;
        
            float blurryness = 5f;
            while (blurryness > 0f)
            {
                blurryness -= 0.05f;
                dof.focusDistance.value = blurryness;
                yield return new WaitForSeconds(0.05f);
            }

            EndGame.instance.LoseGame("Did you know humans need air to survive?");
        }
        
        GameData.airDrained = true;
        
    }
    
}
